
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 10/28/2016 01:55:52
-- Generated from EDMX file: C:\Users\FranciscoyKatherine\Documents\biblioteca.net\SolucionBiblioteca\Biblioteca.DAL\ModeloBiblioteca.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [biblioteca];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AutorLibro]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Libro] DROP CONSTRAINT [FK_AutorLibro];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientePrestamo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Prestamo] DROP CONSTRAINT [FK_ClientePrestamo];
GO
IF OBJECT_ID(N'[dbo].[FK_GeneroLibro]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Libro] DROP CONSTRAINT [FK_GeneroLibro];
GO
IF OBJECT_ID(N'[dbo].[FK_PrestamoLibro]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Prestamo] DROP CONSTRAINT [FK_PrestamoLibro];
GO
IF OBJECT_ID(N'[dbo].[FK_SancionPrestamo]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sancion] DROP CONSTRAINT [FK_SancionPrestamo];
GO
IF OBJECT_ID(N'[dbo].[FK_SancionTipo_Sancion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sancion] DROP CONSTRAINT [FK_SancionTipo_Sancion];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Autor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Autor];
GO
IF OBJECT_ID(N'[dbo].[Cliente]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cliente];
GO
IF OBJECT_ID(N'[dbo].[Genero]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Genero];
GO
IF OBJECT_ID(N'[dbo].[Libro]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Libro];
GO
IF OBJECT_ID(N'[dbo].[Prestamo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Prestamo];
GO
IF OBJECT_ID(N'[dbo].[Sancion]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sancion];
GO
IF OBJECT_ID(N'[dbo].[Tipo_Sancion]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tipo_Sancion];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Autor'
CREATE TABLE [dbo].[Autor] (
    [Id_autor] int  NOT NULL,
    [Nombre] nvarchar(30)  NOT NULL,
    [Nacionalidad] nvarchar(30)  NOT NULL,
    [Sexo] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'Cliente'
CREATE TABLE [dbo].[Cliente] (
    [Id_cliente] int  NOT NULL,
    [Rut] nvarchar(9)  NOT NULL,
    [Nombre_cliente] nvarchar(30)  NOT NULL
);
GO

-- Creating table 'Genero'
CREATE TABLE [dbo].[Genero] (
    [Id_genero] int  NOT NULL,
    [Nombre_genero] nvarchar(15)  NOT NULL
);
GO

-- Creating table 'Libro'
CREATE TABLE [dbo].[Libro] (
    [Id_libro] int  NOT NULL,
    [ISBN] nvarchar(15)  NOT NULL,
    [Titulo] nvarchar(30)  NOT NULL,
    [Estado] int  NOT NULL,
    [Editorial] nvarchar(40)  NOT NULL,
    [AutorId_autor] int  NOT NULL,
    [GeneroId_genero] int  NOT NULL
);
GO

-- Creating table 'Prestamo'
CREATE TABLE [dbo].[Prestamo] (
    [Id_prestamo] int  NOT NULL,
    [Fecha_prestamo] datetime  NOT NULL,
    [Fecha_vencimiento] datetime  NOT NULL,
    [Fecha_entrega] datetime  NOT NULL,
    [SancionId_sancion] int  NOT NULL,
    [ClienteId_cliente] int  NOT NULL,
    [LibroId_libro] int  NOT NULL
);
GO

-- Creating table 'Sancion'
CREATE TABLE [dbo].[Sancion] (
    [Id_sancion] int  NOT NULL,
    [Fecha_sancion] datetime  NOT NULL,
    [Tipo_SancionId_tiposancion] int  NOT NULL
);
GO

-- Creating table 'Tipo_Sancion'
CREATE TABLE [dbo].[Tipo_Sancion] (
    [Id_tiposancion] int  NOT NULL,
    [Nombre_sancion] nvarchar(30)  NOT NULL,
    [Descripcion] nvarchar(200)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_autor] in table 'Autor'
ALTER TABLE [dbo].[Autor]
ADD CONSTRAINT [PK_Autor]
    PRIMARY KEY CLUSTERED ([Id_autor] ASC);
GO

-- Creating primary key on [Id_cliente] in table 'Cliente'
ALTER TABLE [dbo].[Cliente]
ADD CONSTRAINT [PK_Cliente]
    PRIMARY KEY CLUSTERED ([Id_cliente] ASC);
GO

-- Creating primary key on [Id_genero] in table 'Genero'
ALTER TABLE [dbo].[Genero]
ADD CONSTRAINT [PK_Genero]
    PRIMARY KEY CLUSTERED ([Id_genero] ASC);
GO

-- Creating primary key on [Id_libro] in table 'Libro'
ALTER TABLE [dbo].[Libro]
ADD CONSTRAINT [PK_Libro]
    PRIMARY KEY CLUSTERED ([Id_libro] ASC);
GO

-- Creating primary key on [Id_prestamo] in table 'Prestamo'
ALTER TABLE [dbo].[Prestamo]
ADD CONSTRAINT [PK_Prestamo]
    PRIMARY KEY CLUSTERED ([Id_prestamo] ASC);
GO

-- Creating primary key on [Id_sancion] in table 'Sancion'
ALTER TABLE [dbo].[Sancion]
ADD CONSTRAINT [PK_Sancion]
    PRIMARY KEY CLUSTERED ([Id_sancion] ASC);
GO

-- Creating primary key on [Id_tiposancion] in table 'Tipo_Sancion'
ALTER TABLE [dbo].[Tipo_Sancion]
ADD CONSTRAINT [PK_Tipo_Sancion]
    PRIMARY KEY CLUSTERED ([Id_tiposancion] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AutorId_autor] in table 'Libro'
ALTER TABLE [dbo].[Libro]
ADD CONSTRAINT [FK_AutorLibro]
    FOREIGN KEY ([AutorId_autor])
    REFERENCES [dbo].[Autor]
        ([Id_autor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AutorLibro'
CREATE INDEX [IX_FK_AutorLibro]
ON [dbo].[Libro]
    ([AutorId_autor]);
GO

-- Creating foreign key on [GeneroId_genero] in table 'Libro'
ALTER TABLE [dbo].[Libro]
ADD CONSTRAINT [FK_GeneroLibro]
    FOREIGN KEY ([GeneroId_genero])
    REFERENCES [dbo].[Genero]
        ([Id_genero])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GeneroLibro'
CREATE INDEX [IX_FK_GeneroLibro]
ON [dbo].[Libro]
    ([GeneroId_genero]);
GO

-- Creating foreign key on [Tipo_SancionId_tiposancion] in table 'Sancion'
ALTER TABLE [dbo].[Sancion]
ADD CONSTRAINT [FK_Tipo_SancionSancion]
    FOREIGN KEY ([Tipo_SancionId_tiposancion])
    REFERENCES [dbo].[Tipo_Sancion]
        ([Id_tiposancion])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Tipo_SancionSancion'
CREATE INDEX [IX_FK_Tipo_SancionSancion]
ON [dbo].[Sancion]
    ([Tipo_SancionId_tiposancion]);
GO

-- Creating foreign key on [SancionId_sancion] in table 'Prestamo'
ALTER TABLE [dbo].[Prestamo]
ADD CONSTRAINT [FK_SancionPrestamo]
    FOREIGN KEY ([SancionId_sancion])
    REFERENCES [dbo].[Sancion]
        ([Id_sancion])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SancionPrestamo'
CREATE INDEX [IX_FK_SancionPrestamo]
ON [dbo].[Prestamo]
    ([SancionId_sancion]);
GO

-- Creating foreign key on [ClienteId_cliente] in table 'Prestamo'
ALTER TABLE [dbo].[Prestamo]
ADD CONSTRAINT [FK_ClientePrestamo]
    FOREIGN KEY ([ClienteId_cliente])
    REFERENCES [dbo].[Cliente]
        ([Id_cliente])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientePrestamo'
CREATE INDEX [IX_FK_ClientePrestamo]
ON [dbo].[Prestamo]
    ([ClienteId_cliente]);
GO

-- Creating foreign key on [LibroId_libro] in table 'Prestamo'
ALTER TABLE [dbo].[Prestamo]
ADD CONSTRAINT [FK_LibroPrestamo]
    FOREIGN KEY ([LibroId_libro])
    REFERENCES [dbo].[Libro]
        ([Id_libro])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LibroPrestamo'
CREATE INDEX [IX_FK_LibroPrestamo]
ON [dbo].[Prestamo]
    ([LibroId_libro]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------