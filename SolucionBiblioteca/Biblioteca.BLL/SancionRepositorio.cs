﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class SancionRepositorio
    {

        public bool Create(Sancion sancionBO) {
            try
            {
                DAL.Sancion sancionDAL = new DAL.Sancion();
                sancionDAL.Id_sancion = sancionBO.Id;
                sancionDAL.Fecha_sancion = sancionBO.FechaSancion;

                CommonBC.ModeloEntities.AddToSancion(sancionDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Read(int id) {
            try
            {
                DAL.Sancion sancionDAL = CommonBC.ModeloEntities.Sancion.FirstOrDefault(a => a.Id_sancion == id);
                Sancion sancionBO = new Sancion();
                sancionBO.FechaSancion = sancionDAL.Fecha_sancion;

                return true;
            
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Sancion sancionBO) {
            try
            {
                DAL.Sancion sancionDAL = CommonBC.ModeloEntities.Sancion.FirstOrDefault(a => a.Id_sancion == sancionBO.Id);
                sancionDAL.Id_sancion = sancionBO.Id;
                sancionDAL.Fecha_sancion = sancionBO.FechaSancion;

                CommonBC.ModeloEntities.SaveChanges();

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id) {
            try
            {
                DAL.Sancion sancionDAL = CommonBC.ModeloEntities.Sancion.FirstOrDefault(a=>a.Id_sancion ==id);

                CommonBC.ModeloEntities.DeleteObject(sancionDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DAL.Sancion> ReadAll() {
            return CommonBC.ModeloEntities.Sancion.ToList();
        }

    }
}
