﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class Prestamo
    {
        public int Id { get; set; }
        public int IdLibro { get; set; }
        public int IdCliente { get; set; }
        public DateTime FechaPrestamo { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime FechaEntrega { get; set; }

        public Prestamo()
        {
            Init();
        }

        private void Init() {
            Id = 0;
            IdLibro = 0;
            IdCliente = 0;
            FechaEntrega = DateTime.Today;
            FechaPrestamo = DateTime.Today;
            FechaVencimiento = DateTime.Today;
        }

    }
}
