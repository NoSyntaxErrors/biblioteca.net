﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class AutorRepositorio
    {

        #region CRUD

        public Autor Create(Autor autor)
        {
            try
            {
                DAL.Autor autorDAL = new DAL.Autor();
                autorDAL.Id_autor = autor.Id;
                autorDAL.Nombre = autor.Nombre;
                autorDAL.Nacionalidad = autor.Nacionalidad;
                autorDAL.Sexo = autor.Sexo.ToString();

                CommonBC.ModeloEntities.AddToAutor(autorDAL);
                CommonBC.ModeloEntities.SaveChanges();
                return autor;

            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Read(int id)
        {
            try
            {
                DAL.Autor autorDAL = CommonBC.ModeloEntities.Autor.FirstOrDefault(a => a.Id_autor == id);
                Autor autor = new Autor();

                autor.Id = autorDAL.Id_autor;
                autor.Nombre = autorDAL.Nombre;
                autor.Nacionalidad = autorDAL.Nacionalidad;
                autor.Sexo = (TipoSexo)Enum.Parse(typeof(TipoSexo), autorDAL.Sexo);

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool Update(Autor autor)
        {
            try
            {
                DAL.Autor autorDAL = CommonBC.ModeloEntities.Autor.FirstOrDefault(a => a.Id_autor == autor.Id);
                autorDAL.Id_autor = autor.Id;
                autorDAL.Nombre = autor.Nombre;
                autorDAL.Nacionalidad = autor.Nacionalidad;
                autorDAL.Sexo = autor.Sexo.ToString();

                CommonBC.ModeloEntities.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                DAL.Autor autorDAL = CommonBC.ModeloEntities.Autor.FirstOrDefault(a => a.Id_autor == id);
                CommonBC.ModeloEntities.DeleteObject(autorDAL);
                CommonBC.ModeloEntities.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }

        }

        public List<DAL.Autor> ReadAll()
        {
            return CommonBC.ModeloEntities.Autor.ToList();
        }

        #endregion


    }
}
