﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class GeneroRepositorio
    {

        public bool Create(Genero genero) {
            try
            {
                DAL.Genero generoDAL = new DAL.Genero();
                generoDAL.Id_genero = genero.Id_genero;
                generoDAL.Nombre_genero = genero.Nombre_genero;

                CommonBC.ModeloEntities.AddToGenero(generoDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Read(int id) {
            try
            {
                DAL.Genero generoDal = CommonBC.ModeloEntities.Genero.FirstOrDefault(a => a.Id_genero == id);
                Genero genero = new Genero();
                genero.Id_genero = generoDal.Id_genero;
                genero.Nombre_genero = generoDal.Nombre_genero;
                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Genero genero) {
            try
            {
                DAL.Genero generoDal = CommonBC.ModeloEntities.Genero.FirstOrDefault(a=> a.Id_genero== genero.Id_genero);
                generoDal.Nombre_genero = genero.Nombre_genero;
                CommonBC.ModeloEntities.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool Delete(int id) {
            try
            {
                DAL.Genero generoDal = CommonBC.ModeloEntities.Genero.FirstOrDefault(a => a.Id_genero == id);
                CommonBC.ModeloEntities.DeleteObject(generoDal);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DAL.Genero> ReadAll() {
            return CommonBC.ModeloEntities.Genero.ToList();
        }
    }
}
