﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class Libro
    {
        public int Id { get; set; }
        public string Isbn { get; set; }
        public string Titulo { get; set; }
        public string Editorial { get; set; }
        public int Estado { get; set; }
        public int GeneroId { get; set; }
        public int AutorId { get; set; }

        public Libro()
        {
            this.Init();
        }

        private void Init()
        {
            Id = 0;
            Isbn = string.Empty;
            Titulo = string.Empty;
            Editorial = string.Empty;
            Estado = 0;
            GeneroId = 0;
            AutorId = 0;
        }
    }
}
