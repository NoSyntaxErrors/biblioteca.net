﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class ClienteRepositorio
    {

        public bool Create(Cliente clienteBO) {
            try
            {
                DAL.Cliente clienteDal = new DAL.Cliente();
                clienteDal.Id_cliente = clienteBO.Id;
                clienteDal.Rut = clienteBO.Rut;
                clienteDal.Nombre_cliente = clienteBO.Nombre;

                CommonBC.ModeloEntities.AddToCliente(clienteDal);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Read(int id)   {
            try
            {
                DAL.Cliente clienteDal = CommonBC.ModeloEntities.Cliente.FirstOrDefault(a => a.Id_cliente == id);
                Cliente cliente = new Cliente();

                cliente.Rut      = clienteDal.Rut;
                cliente.Nombre   = clienteDal.Nombre_cliente;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Cliente clienteBO) {
            try
            {
                DAL.Cliente clienteDal = CommonBC.ModeloEntities.Cliente.FirstOrDefault(a => a.Id_cliente == clienteBO.Id);
                clienteDal.Rut = clienteBO.Rut;
                clienteDal.Nombre_cliente = clienteBO.Nombre;

                CommonBC.ModeloEntities.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;   
            }
        }

        public bool Delete(int id) {
            try
            {
                DAL.Cliente clienteDal = CommonBC.ModeloEntities.Cliente.FirstOrDefault(a => a.Id_cliente == id);
                CommonBC.ModeloEntities.DeleteObject(clienteDal);
                CommonBC.ModeloEntities.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DAL.Cliente> ReadAll() {
            return CommonBC.ModeloEntities.Cliente.ToList();
        }

    }
}
