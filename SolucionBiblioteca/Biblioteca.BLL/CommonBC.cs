﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Biblioteca.DAL;

namespace Biblioteca.BLL
{
    public static class CommonBC
    {
        private static bibliotecaEntities _bibliotecaEntities;

        public static bibliotecaEntities ModeloEntities {
            get { 
                if(_bibliotecaEntities==null){
                    _bibliotecaEntities = new bibliotecaEntities();
                }
                return _bibliotecaEntities;
            }
        }
    }
}
