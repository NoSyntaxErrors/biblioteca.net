﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class LibroRepositorio
    {

        public Libro Create(Libro libroBO) {
            try
            {
                DAL.Libro libroDal = new DAL.Libro();
                libroDal.Id_libro = libroBO.Id;
                libroDal.ISBN = libroBO.Isbn;
                libroDal.Titulo = libroBO.Titulo;
                libroDal.Editorial = libroBO.Editorial;
                libroDal.Estado = libroBO.Estado;
                libroDal.GeneroId_genero = libroBO.GeneroId;
                libroDal.AutorId_autor = libroBO.AutorId;
              

                CommonBC.ModeloEntities.AddToLibro(libroDal);
                CommonBC.ModeloEntities.SaveChanges();

                return libroBO;
            }
            catch (Exception)
            {
                Console.WriteLine("Hola");
                return null;
            }
        }

        public Libro Read(int Id) {
            try
            {
                DAL.Libro libroDal = CommonBC.ModeloEntities.Libro.FirstOrDefault(a => a.Id_libro == Id);
                Libro libroBO = new Libro();
                libroBO.Isbn = libroDal.ISBN;
                libroBO.Titulo = libroDal.Titulo;
                libroBO.Editorial = libroDal.Editorial;
                libroBO.Estado = libroDal.Estado;
                libroBO.AutorId = libroDal.AutorId_autor;
                libroBO.GeneroId = libroDal.GeneroId_genero;


                return libroBO;
                
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Update(Libro libroBO)
        {
            try
            {
                DAL.Libro libroDal = CommonBC.ModeloEntities.Libro.FirstOrDefault(a => a.Id_libro == libroBO.Id);
                libroDal.ISBN = libroBO.Isbn;
                libroDal.Titulo = libroBO.Titulo;
                libroDal.Editorial = libroBO.Editorial;
                libroDal.Estado = libroBO.Estado;
                libroDal.GeneroId_genero = libroBO.GeneroId;
                libroDal.AutorId_autor = libroBO.AutorId;

                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int Id) {
            try
            {
                DAL.Libro libroDal = CommonBC.ModeloEntities.Libro.FirstOrDefault(a => a.Id_libro == Id);
                CommonBC.ModeloEntities.DeleteObject(libroDal);
                CommonBC.ModeloEntities.SaveChanges();
                
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DAL.Libro> ReadAllDAL() {
            return CommonBC.ModeloEntities.Libro.ToList();            
        }

    }
}
