﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class Genero
    {
        public int Id_genero { get; set; }
        public string Nombre_genero { get; set; }


        public Genero() {
            this.Init();
        }

        private void Init(){
            Id_genero = 0;
            Nombre_genero = string.Empty;
        }
    }
}
