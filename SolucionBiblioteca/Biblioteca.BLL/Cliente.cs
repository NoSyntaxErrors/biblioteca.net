﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }

        public Cliente()
        {
            Init();
        }

        private void Init()
        {
            Id = 0;
            Rut = string.Empty;
            Nombre = string.Empty;
        }
    }
}
