﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class LibroCollection
    {
     

        private List<Libro> GenerarListado(List<DAL.Libro> librosDAL)
        {

            List<Libro> libros = new List<Libro>();

            foreach (var libroDAL in librosDAL)
            {
                Libro libro = new Libro();

                libro.Id = libroDAL.Id_libro;
                libro.Isbn = libroDAL.ISBN;
                libro.Titulo = libroDAL.Titulo;
                libro.Editorial = libroDAL.Editorial;
                libro.AutorId = libroDAL.AutorId_autor;
                libro.GeneroId = libroDAL.GeneroId_genero;

                libros.Add(libro);
            }

            return libros;
        }

        public List<Libro> ReadAll()
        {
            LibroRepositorio libroRepo = new LibroRepositorio();
            return GenerarListado(libroRepo.ReadAllDAL());
        }

        public List<Libro> LibrosSinPrestar() {
            return ReadAll().Where(a=>a.Estado ==0).ToList();
        }

        public List<Libro> LibrosPrestados() {
            return ReadAll().Where(a => a.Estado == 1).ToList();
        }

    }
}
