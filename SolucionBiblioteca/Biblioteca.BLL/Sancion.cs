﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class Sancion
    {
        public int Id { get; set; }
        public DateTime FechaSancion { get; set; }
        public int IdPrestamo { get; set; }
        public int IdTipoSancion { get; set; }

        public Sancion()
        {
            Init();
        }

        private void Init() {
            Id = 0;
            FechaSancion = DateTime.Today;
            IdPrestamo = 0;
            IdTipoSancion = 0;
        }
    }
}
