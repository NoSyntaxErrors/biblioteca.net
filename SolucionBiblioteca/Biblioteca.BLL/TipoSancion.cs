﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class TipoSancion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public TipoSancion()
        {
            Init();
        }

        private void Init() {
            Id = 0;
            Nombre = string.Empty;
            Descripcion = string.Empty;
        }
    }
}
