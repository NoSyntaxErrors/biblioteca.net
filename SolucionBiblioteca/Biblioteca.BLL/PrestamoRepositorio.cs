﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class PrestamoRepositorio
    {

        public bool Create(Prestamo prestamoBO) {
            try
            {
                DAL.Prestamo prestamoDAL = new DAL.Prestamo();
                prestamoDAL.Id_prestamo = prestamoBO.Id;
                prestamoDAL.Fecha_prestamo = prestamoBO.FechaPrestamo;
                prestamoDAL.Fecha_entrega = prestamoBO.FechaEntrega;
                prestamoDAL.Fecha_vencimiento = prestamoBO.FechaVencimiento;

                CommonBC.ModeloEntities.AddToPrestamo(prestamoDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Read(int id) {
            try
            {
                DAL.Prestamo prestamoDAL = CommonBC.ModeloEntities.Prestamo.FirstOrDefault(a=> a.Id_prestamo ==id);
                Prestamo prestamoBO = new Prestamo();

                prestamoBO.Id = prestamoDAL.Id_prestamo;
                prestamoBO.FechaPrestamo = prestamoDAL.Fecha_prestamo;
                prestamoBO.FechaEntrega = prestamoDAL.Fecha_entrega;
                prestamoBO.FechaVencimiento = prestamoDAL.Fecha_vencimiento;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        public bool Update(Prestamo prestamoBO) {
            try
            {
                DAL.Prestamo prestamoDAL = new DAL.Prestamo();
                prestamoDAL.Id_prestamo = prestamoBO.Id;
                prestamoDAL.Fecha_prestamo = prestamoBO.FechaPrestamo;
                prestamoDAL.Fecha_entrega = prestamoBO.FechaEntrega;
                prestamoDAL.Fecha_vencimiento = prestamoBO.FechaVencimiento;

                CommonBC.ModeloEntities.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id) {
            try
            {
                DAL.Prestamo prestamo = CommonBC.ModeloEntities.Prestamo.FirstOrDefault(a=> a.Id_prestamo == id);
                CommonBC.ModeloEntities.DeleteObject(prestamo);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DAL.Prestamo> ReadAll() {
            return CommonBC.ModeloEntities.Prestamo.ToList();
        }

    }
}
