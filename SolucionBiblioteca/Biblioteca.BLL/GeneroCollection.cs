﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class GeneroCollection
    {

        private List<Genero> GenerarListado( List<DAL.Genero> generoDAL){
            
            List<Genero> generos = new List<Genero>();

            foreach (var generoD in generoDAL)
            {
                Genero genero = new Genero();

                genero.Id_genero = generoD.Id_genero;
                genero.Nombre_genero = generoD.Nombre_genero;

                generos.Add(genero);
            }

            return generos;
        
        }

        public List<Genero> ReadAll() {
            GeneroRepositorio generoRepo = new GeneroRepositorio();
            return  GenerarListado(generoRepo.ReadAll());
        }
    }
}
