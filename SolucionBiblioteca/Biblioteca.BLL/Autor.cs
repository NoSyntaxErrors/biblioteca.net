﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class Autor
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Nacionalidad { get; set; }
        public TipoSexo Sexo { get; set; }


        public Autor()
        {
            this.Init();
        }

        private void Init()
        {
            Id = 0;
            Nombre = string.Empty;
            Nacionalidad = string.Empty;
            Sexo = TipoSexo.Masculino;
        }

        
    }
}
