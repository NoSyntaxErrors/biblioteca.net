﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class TipoSancionRepositorio
    {

        public bool Create(TipoSancion tipoSancionBO) {
            try
            {
                DAL.Tipo_Sancion tipoSancionDAL = new DAL.Tipo_Sancion();
                tipoSancionDAL.Id_tiposancion = tipoSancionBO.Id;
                tipoSancionDAL.Nombre_sancion = tipoSancionBO.Nombre;
                tipoSancionDAL.Descripcion = tipoSancionBO.Descripcion;

                CommonBC.ModeloEntities.AddToTipo_Sancion(tipoSancionDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Read(int id) {
            try
            {
                DAL.Tipo_Sancion tipoSancionDAL = CommonBC.ModeloEntities.Tipo_Sancion.FirstOrDefault(a => a.Id_tiposancion == id);
                TipoSancion tipoSancionBO = new TipoSancion();
                tipoSancionBO.Id = tipoSancionDAL.Id_tiposancion;
                tipoSancionBO.Nombre = tipoSancionDAL.Nombre_sancion;
                tipoSancionBO.Descripcion = tipoSancionDAL.Descripcion;
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(TipoSancion tipoSancionBO) {
            try
            {
                DAL.Tipo_Sancion tipoSancionDAL = CommonBC.ModeloEntities.Tipo_Sancion.FirstOrDefault(a => a.Id_tiposancion == tipoSancionBO.Id);
           
                tipoSancionDAL.Nombre_sancion = tipoSancionBO.Nombre;
                tipoSancionDAL.Descripcion = tipoSancionBO.Descripcion;

                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id) {
            try
            {
                DAL.Tipo_Sancion tipoSancionDAL = CommonBC.ModeloEntities.Tipo_Sancion.FirstOrDefault(a => a.Id_tiposancion == id);
                CommonBC.ModeloEntities.DeleteObject(tipoSancionDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DAL.Tipo_Sancion> ReadAll() {
            return CommonBC.ModeloEntities.Tipo_Sancion.ToList();
        }

    }
}
