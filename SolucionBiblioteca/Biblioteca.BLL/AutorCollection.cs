﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Biblioteca.BLL
{
    public class AutorCollection
    {
        private List<Autor> GenerarListado(List<DAL.Autor> autorDAL) {
            List<Autor> autores = new List<Autor>();

            foreach (var autorD in autorDAL)
            {
                Autor autor = new Autor();
                autor.Id = autorD.Id_autor;
                autor.Nombre = autorD.Nombre;
                autor.Nacionalidad = autorD.Nacionalidad;
                autor.Sexo = (TipoSexo)Enum.Parse(typeof(TipoSexo),autorD.Sexo);

                autores.Add(autor);
            }
            return autores;

        }

        public List<Autor> ReadAll()
        {
            AutorRepositorio autorRepo = new AutorRepositorio();
            return GenerarListado(autorRepo.ReadAll());
        }

    }
}
