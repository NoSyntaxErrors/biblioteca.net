USE [master]
GO
/****** Object:  Database [biblioteca]    Script Date: 27/10/2016 1:09:51 ******/
CREATE DATABASE [biblioteca] ON  PRIMARY 
( NAME = N'biblioteca', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\biblioteca.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'biblioteca_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\biblioteca_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [biblioteca] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [biblioteca].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [biblioteca] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [biblioteca] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [biblioteca] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [biblioteca] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [biblioteca] SET ARITHABORT OFF 
GO
ALTER DATABASE [biblioteca] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [biblioteca] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [biblioteca] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [biblioteca] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [biblioteca] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [biblioteca] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [biblioteca] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [biblioteca] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [biblioteca] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [biblioteca] SET  DISABLE_BROKER 
GO
ALTER DATABASE [biblioteca] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [biblioteca] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [biblioteca] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [biblioteca] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [biblioteca] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [biblioteca] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [biblioteca] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [biblioteca] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [biblioteca] SET  MULTI_USER 
GO
ALTER DATABASE [biblioteca] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [biblioteca] SET DB_CHAINING OFF 
GO
USE [biblioteca]
GO
/****** Object:  Table [dbo].[Autor]    Script Date: 27/10/2016 1:09:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Autor](
	[Id_autor] [int] NOT NULL,
	[Nombre] [nvarchar](30) NOT NULL,
	[Apellido] [nvarchar](30) NOT NULL,
	[Nacionalidad] [nvarchar](30) NOT NULL,
	[Sexo] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Autor] PRIMARY KEY CLUSTERED 
(
	[Id_autor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 27/10/2016 1:09:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[Id_cliente] [int] NOT NULL,
	[Rut] [nvarchar](9) NOT NULL,
	[Nombre_cliente] [nvarchar](30) NOT NULL,
	[Apellido_cliente] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[Id_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Genero]    Script Date: 27/10/2016 1:09:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genero](
	[Id_genero] [int] NOT NULL,
	[Nombre_genero] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_Genero] PRIMARY KEY CLUSTERED 
(
	[Id_genero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Libro]    Script Date: 27/10/2016 1:09:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Libro](
	[Id_libro] [int] NOT NULL,
	[ISBN] [nvarchar](15) NOT NULL,
	[Titulo] [nvarchar](30) NOT NULL,
	[Estado] [int] NOT NULL,
	[Editorial] [nvarchar](40) NOT NULL,
	[AutorId_autor] [int] NOT NULL,
	[GeneroId_genero] [int] NOT NULL,
 CONSTRAINT [PK_Libro] PRIMARY KEY CLUSTERED 
(
	[Id_libro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Prestamo]    Script Date: 27/10/2016 1:09:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prestamo](
	[Id_prestamo] [int] NOT NULL,
	[Fecha_prestamo] [datetime] NOT NULL,
	[Fecha_vencimiento] [datetime] NOT NULL,
	[Fecha_entrega] [datetime] NOT NULL,
	[LibroId] [int] NOT NULL,
	[ClienteId_cliente] [int] NOT NULL,
	[Libro_Id_libro] [int] NOT NULL,
 CONSTRAINT [PK_Prestamo] PRIMARY KEY CLUSTERED 
(
	[Id_prestamo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sancion]    Script Date: 27/10/2016 1:09:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sancion](
	[Id_sancion] [int] NOT NULL,
	[Fecha_sancion] [datetime] NOT NULL,
	[PrestamoId] [int] NOT NULL,
	[TipoSancionId] [int] NOT NULL,
	[Prestamo_Id_prestamo] [int] NOT NULL,
	[Tipo_Sancion_Id_tiposancion] [int] NOT NULL,
 CONSTRAINT [PK_Sancion] PRIMARY KEY CLUSTERED 
(
	[Id_sancion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tipo_Sancion]    Script Date: 27/10/2016 1:09:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo_Sancion](
	[Id_tiposancion] [int] NOT NULL,
	[Nombre_sancion] [nvarchar](30) NOT NULL,
	[Descripcion] [nvarchar](200) NULL,
 CONSTRAINT [PK_Tipo_Sancion] PRIMARY KEY CLUSTERED 
(
	[Id_tiposancion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_FK_AutorLibro]    Script Date: 27/10/2016 1:09:52 ******/
CREATE NONCLUSTERED INDEX [IX_FK_AutorLibro] ON [dbo].[Libro]
(
	[AutorId_autor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_GeneroLibro]    Script Date: 27/10/2016 1:09:52 ******/
CREATE NONCLUSTERED INDEX [IX_FK_GeneroLibro] ON [dbo].[Libro]
(
	[GeneroId_genero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_ClientePrestamo]    Script Date: 27/10/2016 1:09:52 ******/
CREATE NONCLUSTERED INDEX [IX_FK_ClientePrestamo] ON [dbo].[Prestamo]
(
	[ClienteId_cliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_PrestamoLibro]    Script Date: 27/10/2016 1:09:52 ******/
CREATE NONCLUSTERED INDEX [IX_FK_PrestamoLibro] ON [dbo].[Prestamo]
(
	[Libro_Id_libro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_SancionPrestamo]    Script Date: 27/10/2016 1:09:52 ******/
CREATE NONCLUSTERED INDEX [IX_FK_SancionPrestamo] ON [dbo].[Sancion]
(
	[Prestamo_Id_prestamo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_SancionTipo_Sancion]    Script Date: 27/10/2016 1:09:52 ******/
CREATE NONCLUSTERED INDEX [IX_FK_SancionTipo_Sancion] ON [dbo].[Sancion]
(
	[Tipo_Sancion_Id_tiposancion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Libro]  WITH CHECK ADD  CONSTRAINT [FK_AutorLibro] FOREIGN KEY([AutorId_autor])
REFERENCES [dbo].[Autor] ([Id_autor])
GO
ALTER TABLE [dbo].[Libro] CHECK CONSTRAINT [FK_AutorLibro]
GO
ALTER TABLE [dbo].[Libro]  WITH CHECK ADD  CONSTRAINT [FK_GeneroLibro] FOREIGN KEY([GeneroId_genero])
REFERENCES [dbo].[Genero] ([Id_genero])
GO
ALTER TABLE [dbo].[Libro] CHECK CONSTRAINT [FK_GeneroLibro]
GO
ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_ClientePrestamo] FOREIGN KEY([ClienteId_cliente])
REFERENCES [dbo].[Cliente] ([Id_cliente])
GO
ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_ClientePrestamo]
GO
ALTER TABLE [dbo].[Prestamo]  WITH CHECK ADD  CONSTRAINT [FK_PrestamoLibro] FOREIGN KEY([Libro_Id_libro])
REFERENCES [dbo].[Libro] ([Id_libro])
GO
ALTER TABLE [dbo].[Prestamo] CHECK CONSTRAINT [FK_PrestamoLibro]
GO
ALTER TABLE [dbo].[Sancion]  WITH CHECK ADD  CONSTRAINT [FK_SancionPrestamo] FOREIGN KEY([Prestamo_Id_prestamo])
REFERENCES [dbo].[Prestamo] ([Id_prestamo])
GO
ALTER TABLE [dbo].[Sancion] CHECK CONSTRAINT [FK_SancionPrestamo]
GO
ALTER TABLE [dbo].[Sancion]  WITH CHECK ADD  CONSTRAINT [FK_SancionTipo_Sancion] FOREIGN KEY([Tipo_Sancion_Id_tiposancion])
REFERENCES [dbo].[Tipo_Sancion] ([Id_tiposancion])
GO
ALTER TABLE [dbo].[Sancion] CHECK CONSTRAINT [FK_SancionTipo_Sancion]
GO
USE [master]
GO
ALTER DATABASE [biblioteca] SET  READ_WRITE 
GO
