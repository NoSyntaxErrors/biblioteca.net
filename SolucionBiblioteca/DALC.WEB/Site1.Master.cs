﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biblioteca.Web
{
    public partial class Site1 : System.Web.UI.MasterPage
    {

        public List<Usuario> usuarios
        {
            get {
                if (Session["usuarios"]==null)
                {
                    Session["usuarios"] = new List<Usuario>();
                }
                return (List<Usuario>)Session["usuarios"];
            }

            set {
                Session["usuarios"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            linkButtonCerrar.Visible = false;

            if (usuarios.Count>0)
            {
                lblNombreUsuario.Text = "Que tengas un buen dia " + usuarios.First().Nombre.ToString();
                linkButtonCerrar.Visible = true;
                
                
                pnlInicioSesion.Visible = false;
            }

        }

        public void logout()
        {
            Session.Abandon();
        }

        protected void linkButtonCerrar_Click(object sender, EventArgs e)
        {
            Session.Contents.Abandon();
            Session.Contents.RemoveAll();
            Session.Contents.Clear();
            Server.Transfer("Inicio.aspx");
        }
    }
}