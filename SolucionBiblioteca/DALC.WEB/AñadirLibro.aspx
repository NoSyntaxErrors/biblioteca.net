﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="AñadirLibro.aspx.cs" Inherits="Biblioteca.Web.AñadirLibro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Table ID="Table1" runat="server">
        <asp:TableHeaderRow>
            <asp:TableHeaderCell>
                <asp:Label ID="Label1" runat="server" Text="Ingresar nuevo libro"></asp:Label>
            </asp:TableHeaderCell>
            <asp:TableHeaderCell>

            </asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label7" runat="server" Text="ID"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label2" runat="server" Text="ISBN"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtIsbn" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label3" runat="server" Text="Titulo"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtTitulo" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label5" runat="server" Text="Categoria"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="ddlCategoria" runat="server">
                </asp:DropDownList>            
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label6" runat="server" Text="Editoral"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtEditorial" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label4" runat="server" Text="Autor"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
            <asp:DropDownList ID="ddlAutor" runat="server">
                </asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnTable2" runat="server" Text="Agregar nuevo autor" OnClick="btnCrearAutor1_Click" />
            
            </asp:TableCell>
        </asp:TableRow>

    </asp:Table>

    <br />
    <asp:Table ID="Table2" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell>
            
            <asp:Label ID="Label8" runat="server" Text="Agregar Autor"></asp:Label>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
        </asp:TableHeaderRow>
        
        <asp:TableRow>
            <asp:TableCell>
               <asp:Label runat="server" Text="ID"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtIDAutor" runat="server"></asp:TextBox>
            </asp:TableCell>
            
            <asp:TableCell>
                <asp:Label runat="server" Text="Nacionalidad"></asp:Label>
            </asp:TableCell>
            
            <asp:TableCell>
                <asp:TextBox ID="txtNacionalidad" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label9" runat="server" Text="Nombre"></asp:Label>
            </asp:TableCell>

            <asp:TableCell>
                <asp:TextBox ID="txtNombreAutor" runat="server"></asp:TextBox>
            </asp:TableCell>

           <asp:TableCell>
                <asp:Label ID="Label11" runat="server" Text="Sexo"></asp:Label>
            </asp:TableCell>
            
            <asp:TableCell>
                <asp:DropDownList ID="ddlSexo" runat="server">
                </asp:DropDownList>
            </asp:TableCell>

        </asp:TableRow>
        <asp:TableRow>
            
            <asp:TableCell>
                <asp:Button ID="btnCrearAutor" runat="server" Text="Crear Autor" OnClick="btnCrearAutor_Click"/>
            </asp:TableCell>

            <asp:TableCell>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
            </asp:TableCell>
        </asp:TableRow>
    
    </asp:Table>
  
    
    <asp:Button ID="btnCreate" runat="server" Text="Guardar" OnClick="btnCreate_Click" />
    <asp:Button ID="btnConsultar" runat="server" Text="Consultar" 
        onclick="btnConsultar_Click" />
    <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" 
        onclick="btnActualizar_Click" />
    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" 
        onclick="btnEliminar_Click" />
</asp:Content>
