﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="InicioSesion.aspx.cs" Inherits="Biblioteca.Web.InicioSesion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Table ID="Table1" runat="server" BorderColor="White" BorderStyle="Groove" 
    ForeColor="White">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
        </asp:TableCell>

        <asp:TableCell>
            <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
        </asp:TableCell>

        <asp:TableCell>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNombre" ErrorMessage="Campo Requerido"></asp:RequiredFieldValidator>
        </asp:TableCell>

    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="lblNombreUsuario" runat="server" Text="Nombre Usuario"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtNombreUsuario" runat="server"></asp:TextBox>
        </asp:TableCell>

        <asp:TableCell>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNombreUsuario" ErrorMessage="Campo Requerido">        </asp:RequiredFieldValidator>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="lblContraseña" runat="server" Text="Contraseña"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtContraseña" runat="server" TextMode="Password"></asp:TextBox>
        </asp:TableCell>

        <asp:TableCell>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtContraseña" ErrorMessage="Campo Requerido">
        </asp:RequiredFieldValidator>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="lblContraseña2" runat="server" Text="Repetir contraseña"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtContraseña2" runat="server" TextMode="Password"></asp:TextBox>
        </asp:TableCell>

        <asp:TableCell>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Contraseñas no coinciden" ControlToCompare="txtContraseña" ControlToValidate="txtContraseña2"></asp:CompareValidator>
        
        </asp:RequiredFieldValidator>
        
        </asp:TableCell>
    </asp:TableRow>

    </asp:Table>

    <asp:Button ID="btnRegistrar" runat="server" Text="Registrarse" 
    onclick="btnRegistrar_Click" />

</asp:Content>
