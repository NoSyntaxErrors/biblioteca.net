﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biblioteca.BLL;

namespace Biblioteca.Web
{
    public partial class MostrarLibrosPrestado : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void CargarDatos()
        {
            LibroCollection libroCole = new LibroCollection();
            gvLibros.DataSource = libroCole.LibrosPrestados();
            gvLibros.DataBind();
        }

        



    }
}