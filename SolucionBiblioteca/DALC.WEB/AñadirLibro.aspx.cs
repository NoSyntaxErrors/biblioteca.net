﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biblioteca.BLL;

namespace Biblioteca.Web
{
    public partial class AñadirLibro : System.Web.UI.Page
    {
        LibroRepositorio libroRepo = new LibroRepositorio();

        protected void Page_Load(object sender, EventArgs e)
        {
            cargarDatos();
            Table2.Visible = false;
            ddlAutor.DataBind();
        }

        private void limpiarControles()
        {
            txtId.Text = string.Empty;
            txtIsbn.Text = string.Empty;
            txtTitulo.Text = string.Empty;
            txtEditorial.Text = string.Empty;
            ddlCategoria.SelectedIndex = 0;


        }

        void cargarDatos() { 
            GeneroCollection geneCollection = new GeneroCollection();
            ddlCategoria.DataSource = geneCollection.ReadAll() ;
            ddlCategoria.DataTextField = "Nombre_genero";
            ddlCategoria.DataValueField = "Id_genero";
            ddlCategoria.DataBind();

            AutorCollection autorCollection = new AutorCollection();
            ddlAutor.DataSource = autorCollection.ReadAll();
            ddlAutor.DataTextField = "Nombre";
            ddlAutor.DataValueField = "Id";
            ddlAutor.DataBind();

            ddlSexo.DataSource = Enum.GetValues(typeof(TipoSexo));
            ddlSexo.DataBind();

        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                Libro libroBO = new Libro();

                libroBO.Id = int.Parse(txtId.Text);
                libroBO.Isbn = txtIsbn.Text;
                libroBO.Titulo = txtTitulo.Text;
                libroBO.Editorial = txtEditorial.Text;
                libroBO.Estado = 0;
                libroBO.AutorId = int.Parse(ddlAutor.SelectedValue);
                libroBO.GeneroId = int.Parse(ddlCategoria.SelectedValue);
                
                libroRepo.Create(libroBO);
                limpiarControles();
            }
            catch (Exception ex)
            {
                Console.WriteLine("asdasd");
                Console.WriteLine(ex.Message); ;
            }
        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                Libro libroBO = new Libro();
                libroRepo.Read(int.Parse(txtId.Text));

                txtId.Text = libroBO.Id.ToString();
                txtIsbn.Text = libroBO.Isbn;
                txtTitulo.Text = libroBO.Titulo;
                txtEditorial.Text = libroBO.Editorial;
                ddlCategoria.SelectedIndex = libroBO.GeneroId;
                ddlAutor.SelectedIndex = libroBO.AutorId;

                    
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Libro libroBO = new Libro();

                libroBO.Id = int.Parse(txtId.Text);
                libroBO.Isbn = txtIsbn.Text;
                libroBO.Titulo = txtTitulo.Text;
                libroBO.Editorial = txtEditorial.Text;
                libroBO.GeneroId = ddlCategoria.SelectedIndex;
                libroBO.AutorId = ddlAutor.SelectedIndex;

                libroRepo.Update(libroBO);
                limpiarControles();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                libroRepo.Delete(int.Parse(txtId.Text));
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        protected void btnCrearAutor1_Click(object sender, EventArgs e)
        {
            Table2.Visible = true;
        }

        protected void btnCrearAutor_Click(object sender, EventArgs e)
        {
            try
            {
                AutorRepositorio autorRepo = new AutorRepositorio();
                Autor autor = new Autor();

                autor.Id = int.Parse(txtIDAutor.Text);
                autor.Nombre = txtNombreAutor.Text;
                autor.Nacionalidad = txtNacionalidad.Text;
                autor.Sexo = (TipoSexo) ddlSexo.SelectedIndex;

                autorRepo.Create(autor);
                ddlAutor.DataBind();

                Response.Redirect("AñadirLibro.aspx");

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Table2.Visible = false;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }


    }
}