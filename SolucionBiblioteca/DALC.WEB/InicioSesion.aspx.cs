﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biblioteca.Web
{
    public partial class InicioSesion : System.Web.UI.Page
    {

        public List<Usuario> usuarios
        {

            get
            {
                if (Session["usuarios"] == null)
                {
                    Session["usuarios"] = new List<Usuario>();
                }
                return (List<Usuario>)Session["usuarios"];
            }

            set
            {
                Session["usuarios"] = value;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
             
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();

            usuario.Nombre = txtNombre.Text;
            usuario.NombreUsuario = txtNombreUsuario.Text;
            usuario.Contraseña = txtContraseña.Text;


            if (txtContraseña.Text == txtContraseña2.Text)
            {
                usuarios.Add(usuario);
                Server.Transfer("Inicio.aspx");

            }
            else
            {
                
                txtContraseña.Text = string.Empty;
                txtContraseña2.Text = string.Empty;
            }
        }
    }
}