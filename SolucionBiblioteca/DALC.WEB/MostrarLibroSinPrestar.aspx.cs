﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biblioteca.BLL;

namespace Biblioteca.Web
{
    public partial class MostrarLibroSinPrestar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                CargarDatos();
            }
        }

        private void CargarDatos() {
            LibroCollection libroCole = new LibroCollection();
            gvwLibros.DataSource = libroCole.LibrosSinPrestar();
            gvwLibros.DataBind();
        }
    }
}