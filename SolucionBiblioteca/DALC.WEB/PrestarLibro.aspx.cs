﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biblioteca.Web
{
    public partial class PrestarLibro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                mostrarDatos();
            }
        }

        private void mostrarDatos()
        {
            
        }

        protected void gvwLibros_RowEditing(object sender, GridViewEditEventArgs e)
        {
           
            gvwLibros.EditIndex = e.NewEditIndex;
            mostrarDatos();

        }


        protected void gvwLibros_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

 



    }
}